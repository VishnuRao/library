  <%@ page import="java.util.*,library.model.User"%>
  <%User user=(User)session.getAttribute("user"); %>
   <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="index">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a class="page-scroll" href="#page-top"></a>
                    </li>
                     <%if(user==null){ %>
                    <li>
                        <a class="page-scroll" href="register">Registration</a>
                    </li>
                    <%} %>
                    <li>
                        <a class="page-scroll" href="about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="contact">Contact</a>
                    </li>
                     <%if(user!=null){ %>
                        <li>
	                        <a class="page-scroll" href="home">My Account</a>
	                    </li>
	                    <li>
	                        <a class="page-scroll" href="logout">logout</a>
	                    </li>
                      <%} %>
                </ul>
                <%if(session.getAttribute("user")!=null){ %>
                 <ul class="nav navbar-nav" style="float:right">
                 <%User u= (User)session.getAttribute("user");%>
                    <li>
                        <a class="page-scroll" href="contact">Welcome <%=u.getName()%></a>
                    </li>
                </ul>
                <%} %>
                 <%if(session.getAttribute("user")==null){ %>
                 <form class="navbar-form navbar-right" role="search" action="login" method="post">
                <div>    
                    <div class="form-group" style="margin-top:0px">
                        <input type="text" class="form-control" name="username" placeholder="Username">
                    </div>
                    <div class="form-group" style="margin-top:0px">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary preview-add-button" id="signInButton">Login</button>
               	</div>
                    <!-- <a href="forgotPassword" style="text-align:center;" class="pull-right">Forgot Password</a> -->
        		</form>
        	<%} %>
            </div>
            <!-- /.navbar-collapse -->
           
        </div>
        <!-- /.container -->
    </nav>
    
