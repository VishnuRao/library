<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import="java.util.*,library.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="common/header.jsp" %>
<style type="text/css">
body{padding-top:100px;}
</style>
</head>
<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<%@include file="common/nav1.jsp" %> 

<div class="container">
    <div class="row">
			<div class="container">
				<div class="form-group">
				<form action="deleteUser" method="post" accept-charset="UTF-8" role="form">
			    		    DELETE USER BY USERNAME:  <input class="form-control" placeholder="username" name="username" type="text">
			    		    <input class="btn btn-sm btn-success" type="submit" style="float:right" value="Delete">
			    </form>	
			    <br><br>	    
			    </div>
				<h2>List of Users</h2>
				<ul class="list-group">
					<%
						List<User> userList=(ArrayList)session.getAttribute("userList");
						for(User u:userList){
					%>
					<li class="list-group-item"><a data-toggle="collapse" href="#id<%=u.getMobile()%>"><%=u.getName()%></a>
						<ul class="list-group collapse" id="id<%=u.getMobile()%>">
							<h4>Details</h4>
							Type : <li class="list-group-item"><a href="#"><%=u.getType()%></a></li>
							Username: <li class="list-group-item"><a href="#"><%=u.getUsername()%></a></li>
						</ul>
					</li>
					<%} %>
					
					
				</ul>
			</div>
		</div>
</div>
<%@include file="common/footer.jsp" %>
</body>

</html>