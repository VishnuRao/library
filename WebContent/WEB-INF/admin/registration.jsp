<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="common/header.jsp" %>
<style type="text/css">
body{padding-top:100px;}
</style>
</head>
<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<%@include file="common/nav1.jsp" %> 

<div class="container">
    <div class="row">
		<div class="col-md-4 col-md-offset-4">
    		<div class="panel panel-default">
			  	<div class="panel-heading">
			    	<h3 class="panel-title">Enter User Details</h3>
			 	</div>
			  	<div class="panel-body">
			    	<form action="register" method="post" accept-charset="UTF-8" role="form">
                    <fieldset>
                    	<div class="form-group">
			    		    <input class="form-control" placeholder="Name" name="name" type="text">
			    		</div>
			    		<div class="form-group">
			    		    <input class="form-control" placeholder="Mobile no" name="mobile" type="number">
			    		</div>
			    	  	<div class="form-group">
			    		    <input class="form-control" placeholder="E-mail" name="username" type="text">
			    		</div>
			    		<div class="form-group">
			    			<input class="form-control" placeholder="Password" name="password" type="password" value="">
			    		</div>
			    		  <div class="form-group">
                                    <label for="filter">Filter by</label>
                                    <select class="form-control" name="type">
                                        <option value="patron">Patron</option>
                                        <option value="librarian">Librarian</option>
                                        <option value="admin">Admin</option>
                                    </select>
                                  </div>
			    		<input class="btn btn-lg btn-success btn-block" type="submit" value="Add">
			    	</fieldset>
			      	</form>
			    </div>
			</div>
		</div>
	</div>
</div>
<%@include file="common/footer.jsp" %>
</body>

</html>