<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,library.model.*,library.Dao.*"%>
 <%
 MaterialDaoImpl material=new MaterialDaoImpl();
 UserDaoImpl user=new UserDaoImpl();
 List<UserMaterial> userMaterial=(ArrayList)session.getAttribute("userMaterialList"); 
 
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="common/header.jsp" %>
<style type="text/css">
body{padding-top:100px;}
</style>
</head>
<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<%@include file="common/nav1.jsp" %> 

<div class="container">
    <div class="row">
		<div class="col-md-6 col-md-offset-3">
    		<div class="panel panel-default">
			  	<div class="panel-heading">
			    	<h3 class="panel-title">Pending Materials From Users</h3>
			 	</div>
			  	<div class="panel-body">
			    	<form id="myForm" action="returnedByUser" method="post" accept-charset="UTF-8" role="form">
                    <fieldset>
                    	<div class="form-group" style="color:black">
                                      <%for(UserMaterial list:userMaterial){ 
                                    	  Material m=material.getGetId(list.getMaterial());
                                    	  User u=user.getGetId(list.getUser());
                                      %>
                                        
                                        Material: <label Style="margin-right:30px" for="filter"><%=m.getName() %></label>
                                        User : <label  Style="margin-right:30px" for="filter"><%=u.getUsername() %></label>
                                        <input Style="margin-left:30px;float:right" class="btn btn-sm btn-primary" type="button" onclick="removeFromRecord('<%=u.getId() %>','<%=m.getId()%>')" value="Returned">
                                        <br><br>
                                        <input type="hidden" value="<%=u.getId()%>" name="user">
                                        <input type="hidden" value="<%=m.getId()%>" name="material">
                                      <%} %>                                      
                            </div>
			    	</fieldset>
			      	</form>
			    </div>
			</div>
		</div>
	</div>
</div>
<%@include file="common/footer.jsp" %>
</body>
<script type="text/javascript">
	function removeFromRecord(user,material){
		v=confirm("Do you want to remove this?");
		if(v)
		document.getElementById("myForm").submit();
	}
</script>
</html>