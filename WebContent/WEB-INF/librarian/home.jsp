<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ page import="java.util.*,library.model.User"%>
     <%User user=(User)session.getAttribute("user"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="common/header.jsp" %>
<style type="text/css">
body{padding-top:100px;}
</style>
</head>
<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<%@include file="common/nav1.jsp" %> 

<div class="container">
    <div class="row">
		<div class="col-md-6 col-md-offset-3">
    		<div class="panel panel-default">
			  	<div class="panel-body">
			    	<h1>Welcome user: <%=user.getName()%></h1>
			    	</ul>
			    </div>
			</div>
		</div>
	</div>
</div>
<%@include file="common/footer.jsp" %>
</body>

</html>