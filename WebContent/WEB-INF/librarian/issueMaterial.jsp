<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,library.model.User,library.model.Material"%>
 <%List<User> user=(ArrayList)session.getAttribute("use"); 
 List<Material> material=(ArrayList)session.getAttribute("mat"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="common/header.jsp" %>
<style type="text/css">
body{padding-top:100px;}
</style>
</head>
<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<%@include file="common/nav1.jsp" %> 

<div class="container">
    <div class="row">
		<div class="col-md-4 col-md-offset-4">
    		<div class="panel panel-default">
			  	<div class="panel-heading">
			    	<h3 class="panel-title">Assign Material To User</h3>
			 	</div>
			  	<div class="panel-body">
			    	<form action="issueMaterial" method="post" accept-charset="UTF-8" role="form">
                    <fieldset>
                    	<div class="form-group">
                                    <label for="filter">User</label>
                                    <select class="form-control" name="user">
                                      <%for(User u:user){ %>
                                        <option value="<%=u.getId()%>"><%=u.getUsername()%></option>
                                      <%} %>                                      
                                    </select>
                            </div>
                            <div class="form-group">
                                    <label for="filter">Material</label>
                                    <select class="form-control" name="material">
                                      <%for(Material u:material){ %>
                                        <option value="<%=u.getId()%>"><%=u.getName()%></option>
                                      <%} %>                                      
                                    </select>
                            </div>
			    		<input class="btn btn-lg btn-success btn-block" type="submit" value="Submit">
			    	</fieldset>
			      	</form>
			    </div>
			</div>
		</div>
	</div>
</div>
<%@include file="common/footer.jsp" %>
</body>

</html>