<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="common/header.jsp" %>

</head>
<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<%@include file="common/nav.jsp" %> 
<div class="container">
	<div class="inner cover">
        <h1 class="cover-heading">Welcome to Library Mangagment system</h1>

        <p class="lead">It is an interface for a Library to manage its material and issue the materials to patrons
            
        </p>
      </div>
</div>
<%@include file="common/footer.jsp" %>
</body>

</html>