<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ page import="java.util.*,library.model.User,library.model.Material"%>
     <%User user=(User)session.getAttribute("user"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="common/header.jsp" %>
<style type="text/css">
body{padding-top:10px;}
#search {
    float: right;
    margin-top: 9px;
    width: 250px;
}

.search {
    padding: 5px 0;
    width: 230px;
    height: 30px;
    position: relative;
    left: 10px;
    float: left;
    line-height: 22px;
}

    .search input {
        position: absolute;
        width: 0px;
        float: Left;
        margin-left: 210px;
        -webkit-transition: all 0.7s ease-in-out;
        -moz-transition: all 0.7s ease-in-out;
        -o-transition: all 0.7s ease-in-out;
        transition: all 0.7s ease-in-out;
        height: 30px;
        line-height: 18px;
        padding: 0 2px 0 2px;
        border-radius:1px;
    }

        .search:hover input, .search input:focus {
            width: 200px;
            margin-left: 0px;
        }

.btn {
    height: 30px;
    position: absolute;
    right: 0;
    top: 5px;
    border-radius:1px;
}

</style>
</head>
<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<%@include file="common/nav1.jsp" %> 
<div class="container">
		<div class="container">
		  <form action="search" method="post" accept-charset="UTF-8" role="form">
			<div class="row">
				<h2>Search Material</h2><br>
				<div class="search">
					<input type="text" class="form-control input-sm" maxlength="64"	name="name" placeholder="Search" />
					<button type="submit" class="btn btn-primary btn-sm">Search</button>
				</div>
			</div>
			</form>
		</div>
		<br><br><br><br>
		<ul class="list-group">
					<%
						List<Material> materialList=(ArrayList)session.getAttribute("materialSearchList");
						for(Material u:materialList){
					%>
					<li class="list-group-item"><a data-toggle="collapse" href="#id<%=u.getId()%>"><%=u.getName()%></a>
						<ul class="list-group collapse" id="id<%=u.getId()%>">
							<h4>Details</h4>
							Name : <li class="list-group-item"><a href="#"><%=u.getName()%></a></li>
							Description: <li class="list-group-item"><a href="#"><%=u.getDescription()%></a></li>
							Quantity: <li class="list-group-item"><a href="#"><%=u.getQuantity()%></a></li>
						</ul>
					</li>
					<%} %>
				</ul>
			</div>
	</div>
<%@include file="common/footer.jsp" %>
</body>

</html>