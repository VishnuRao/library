<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ page import="java.util.*,library.model.User,library.model.Material"%>
     <%User user=(User)session.getAttribute("user"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="common/header.jsp" %>
<style type="text/css">
body{padding-top:10px;}
</style>
</head>
<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<%@include file="common/nav1.jsp" %> 
<div class="container">
		<ul class="list-group">
					<br><br>
					<h1>My Materials</h1>
					<br><br>
					<%
						List<Material> materialList=(ArrayList)session.getAttribute("myMaterialList");
						for(Material u:materialList){
					%>
					<li class="list-group-item"><a data-toggle="collapse" href="#id<%=u.getId()%>"><%=u.getName()%></a>
						<ul class="list-group collapse" id="id<%=u.getId()%>">
							<h4>Details</h4>
							Name : <li class="list-group-item"><a href="#"><%=u.getName()%></a></li>
							Description : <li class="list-group-item"><a href="#"><%=u.getDescription()%></a></li>
							Quantity: <li class="list-group-item"><a href="#"><%=u.getQuantity()%></a></li>
						</ul>
					</li>
					<%} %>
				</ul>
			</div>
	</div>
<%@include file="common/footer.jsp" %>
</body>

</html>