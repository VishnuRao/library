-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 06, 2016 at 09:15 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id`, `name`, `description`, `quantity`) VALUES
(1, '1', '1', 0),
(2, 'My Java', 'here goes description', 9),
(3, '7', '7', 7),
(4, 'Spring ', 'A to z about spring', 0),
(5, 'Wings of Fire', 'apjadd', 1),
(6, 'sdfsdfsdf', 'sdfsdfsdfsdf', 2),
(7, 'sdfsdf', 'sdfsdfsd', 334),
(8, 'sdfsdfSDF', 'SDFSDF', 334),
(9, 'sdfSDF', 'DFDF', 34),
(10, '33RRDF', 'DSFSDF', 3),
(11, '334GG', '4FEDF', 3),
(12, 'werwerewkk', 'sdfsdfsdf', 3),
(13, 'dsfdsf', 'sdfsdf', 3);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `mobile`, `name`, `type`) VALUES
(5, 'admin', 'admin', '5', '5', 'admin'),
(9, 'patron1', 'patron1', '3', 'patron', 'patron'),
(10, 'librarian', 'librarian', '23', 'librarian', 'librarian'),
(11, '', '', '', '', 'patron'),
(12, '', '', '', '', 'patron'),
(13, '', '', '', '', 'patron'),
(14, '', '', '', '', 'patron'),
(15, '', '', '', '', 'patron'),
(16, '', '', '', '', 'patron'),
(17, '', '', '', '', 'patron'),
(18, 'jkk', 'kjrrfgrfg', '3423232323', 'kjj', 'librarian'),
(19, 'sdfds', 'dsfsdfsd', '90988987', 'kklkll', 'librarian'),
(20, 'sdfsdfsdf', 'sdfsdf', '1234567342', 'dsfsdf', 'librarian'),
(21, 'frfrfr', 'rfrfrf', '1222222222', 'rrrfrfrf', 'librarian'),
(22, '', '', '', '', 'patron');

-- --------------------------------------------------------

--
-- Table structure for table `user_material`
--

CREATE TABLE IF NOT EXISTS `user_material` (
  `user` int(11) NOT NULL,
  `material` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_material`
--

INSERT INTO `user_material` (`user`, `material`) VALUES
(9, 2),
(9, 2),
(20, 2),
(9, 2),
(10, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
