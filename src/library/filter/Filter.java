package library.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import library.model.User;
public class Filter implements javax.servlet.Filter {
	 public void doFilter(ServletRequest req, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		 List <String> list=new ArrayList<String>();
		    list.add("register");
		    list.add("about");
		    list.add("contact");
		    list.add("login");
		    list.add("logout");
		    list.add("search");
		    list.add("assignMaterial");
		    list.add("index");
		    list.add("home");
		    list.add("adminRegister");
		    list.add("f");
		    list.add("listUsers");
		    list.add("deleteUser");
		    list.add("myMaterials");
		    list.add("addMaterial");
		    list.add("materialList");
		    list.add("availableMaterial");
		    list.add("issueMaterial");
		    list.add("returnedByUser");
		    
		    
	        HttpServletRequest request = (HttpServletRequest) req;
	        String path = ((HttpServletRequest) req).getRequestURI().substring(((HttpServletRequest) req).getContextPath().length());
	        path=path.replaceFirst("/", "");
	        if(path.matches(".*(css|jpg|png|gif|js)") || list.contains(path))
		    	chain.doFilter(req, response);
	        else if(path=="" || path.equals("")){
	        	HttpSession session = request.getSession(true);
	    		if (session.getAttribute("user") != null) { 
	    			User u=(User) session.getAttribute("user");
	    			if(u.getType().equalsIgnoreCase("admin")){
	    				System.out.println("Filete going to Admin");
	    				req.getRequestDispatcher("/WEB-INF/admin/home.jsp").forward(request,response);
	    			}	
	    			else if(u.getType().equalsIgnoreCase("librarian"))
	    				req.getRequestDispatcher("/WEB-INF/librarian/home.jsp").forward(request,response);
	    			else if(u.getType().equalsIgnoreCase("patron"))
	    				req.getRequestDispatcher("/WEB-INF/patron/home.jsp").forward(request,response);
	    			  return;
	    		}else{
	    			req.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	    			  return;
	    		}
	        }else{
				req.getRequestDispatcher("index").forward(request, response);
			}
	    }
	    public void init(FilterConfig config) throws ServletException {
	        System.out.println("Test Param: ");
	    }
	    public void destroy() {
	    }
}
