package library.utility;
import java.sql.Connection;
import java.sql.DriverManager;
import static library.utility.Provider.*;
public class ConnectionProvider {
	static Connection con=null;
	static{
		try{
			Class.forName(DRIVER);
			con=DriverManager.getConnection(CONNECTION_URL,USERNAME,PASSWORD);
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	public static Connection getCon(){
		return con;
	}
}
