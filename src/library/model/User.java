package library.model;

public class User {
	private int id;
	private String username;
	private String password;
	private String mobile;
	private String name;
	private String type;
	public User(String userName, String password) {
		super();
		this.username = userName;
		this.password = password;
	}
	
	public User(String username, String password, String mobile, String name, String type) {
		super();
		this.username = username;
		this.password = password;
		this.mobile = mobile;
		this.name = name;
		this.type = type;
	}

	public User(int id, String username, String password, String mobile, String name, String type) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.mobile = mobile;
		this.name = name;
		this.type = type;
	}

	public String getUsername() {
		return username;
	}
	public User setUserName(String userName) {
		this.username = userName;
		return this;
	}
	public String getPassword() {
		return password;
	}
	public User setPassword(String password) {
		this.password = password;
		return this;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", mobile=" + mobile + ", name=" + name
				+ ", type=" + type + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
