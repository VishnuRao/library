package library.model;

public class Material {
	private int id;
	private String name;
	private String description;
	private int quantity;
	public String getName() {
		return name;
	}
	public Material(String name, String description, int quantity) {
		super();
		this.name = name;
		this.description = description;
		this.quantity = quantity;
	}

	public Material(int id, String name, String description, int quantity) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.quantity = quantity;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
