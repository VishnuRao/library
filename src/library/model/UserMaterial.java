package library.model;

public class UserMaterial {
	private int user;
	private int material;
	
	public UserMaterial(int user, int material) {
		super();
		this.user = user;
		this.material = material;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public int getMaterial() {
		return material;
	}
	public void setMaterial(int material) {
		this.material = material;
	}
	@Override
	public String toString() {
		return "UserMaterial [user=" + user + ", material=" + material + "]";
	}
}
