package library.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import library.model.UserMaterial;
import library.utility.ConnectionProvider;

public class UserMaterialDaoImpl {
	public int addUserMaterial(UserMaterial UserMaterial) {
		String insertTableSQL = "INSERT INTO user_material"
				+ "(user, material) VALUES"
				+ "(?,?)";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setInt(1,UserMaterial.getUser());		
			ps.setInt(2,UserMaterial.getMaterial());			
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public int deleteUserMaterial(int user,int material) {
		String insertTableSQL ="DELETE from user_material WHERE user = ? and material = ?";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setInt(1,user);		
			ps.setInt(2,material);	
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public List<UserMaterial> getByUser(int user) {
		List<UserMaterial> um=new ArrayList<UserMaterial>();
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement("select * from user_material where user=?");
			ps.setInt(1,user);			
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				um.add(new UserMaterial(rs.getInt("user"),rs.getInt("material")));
			}
			return um;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public List<UserMaterial> listUserMaterials() {
		List<UserMaterial> um=new ArrayList<UserMaterial>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement ps= con.createStatement();
			ResultSet rs=ps.executeQuery("select * from user_material");
			while(rs.next()){
				um.add(new UserMaterial(rs.getInt("user"),rs.getInt("material")));
			}
			return um;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
