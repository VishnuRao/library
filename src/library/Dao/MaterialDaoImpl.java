package library.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import library.model.Material;
import library.model.User;
import library.model.UserMaterial;
import library.utility.ConnectionProvider;

public class MaterialDaoImpl {
	public int addMaterial(Material Material) {
		String insertTableSQL = "INSERT INTO material"
				+ "(NAME, DESCRIPTION,QUANTITY) VALUES"
				+ "(?,?,?)";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setString(1,Material.getName());		
			ps.setString(2,Material.getDescription());	
			ps.setInt(3,Material.getQuantity());		
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		System.out.println("@@ val:  "+status);
		return status;
	}
	public int deleteMaterial(String Materialname) {
		String insertTableSQL ="DELETE from material WHERE name = ?";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setString(1,Materialname);		
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public Material getGetId(int id) {
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement("select * from material where id=?");
			ps.setInt(1,id);			
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				return new Material(rs.getInt("id"),rs.getString("name"),rs.getString("description"),rs.getInt("quantity"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public List<Material> listMaterials() {
		List<Material> MaterialList=new ArrayList<Material>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			ResultSet  rs = stmt.executeQuery("select * from material");
			while(rs.next()){
				MaterialList.add(new Material(rs.getInt("id"),rs.getString("name"),rs.getString("description"),rs.getInt("quantity")));
			}
			return MaterialList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public List<Material> listMyMaterials(int id) {
		List<Material> MaterialList=new ArrayList<Material>();
		UserMaterialDaoImpl dao=new UserMaterialDaoImpl();
		List<UserMaterial> um=dao.getByUser(id);
		String array="( ";
		for(UserMaterial m:um){
			array+=""+m.getMaterial()+", ";
		}
		array+="-9 )";
		System.out.println("@@  "+array);
		try{
			Connection con=ConnectionProvider.getCon();
			Statement ps =con.createStatement();
			ResultSet  rs = ps.executeQuery("select * from material where id in "+array);
			while(rs.next()){
				MaterialList.add(new Material(rs.getInt("id"),rs.getString("name"),rs.getString("description"),rs.getInt("quantity")));
			}
			return MaterialList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public List<Material> searchWithName(String name) {
		List<Material> MaterialList=new ArrayList<Material>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			ResultSet  rs = stmt.executeQuery("select * from material where name like '%"+name+"%'");
			while(rs.next()){
				MaterialList.add(new Material(rs.getInt("id"),rs.getString("name"),rs.getString("description"),rs.getInt("quantity")));
			}
			return MaterialList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public List<Material> listAvailable() {
		List<Material> MaterialList=new ArrayList<Material>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			ResultSet  rs = stmt.executeQuery("select * from material where quantity > 0");
			while(rs.next()){
				MaterialList.add(new Material(rs.getInt("id"),rs.getString("name"),rs.getString("description"),rs.getInt("quantity")));
			}
			return MaterialList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
