package library.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import library.Dao.MaterialDaoImpl;
import library.model.Material;
@WebServlet("/addMaterial")
public class AddMaterial extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/addMaterial.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String description=request.getParameter("description");
		int quantity=Integer.parseInt(request.getParameter("quantity"));
		
		Material material= new Material(name,description,quantity);
		MaterialDaoImpl daoImpl = new MaterialDaoImpl();
		daoImpl.addMaterial(material);
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/addMaterial.jsp").forward(request,response);
	}

}
