package library.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.UserMaterialDaoImpl;
import library.model.UserMaterial;

@WebServlet("/returnedByUser")
public class ReturnedByUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserMaterialDaoImpl dao=new UserMaterialDaoImpl();
		List<UserMaterial> um=dao.listUserMaterials();
		HttpSession session = request.getSession(false);
		 if(session!=null)  
		     session.setAttribute("userMaterialList", um); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/returnedByUser.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int user=Integer.parseInt(request.getParameter("user"));
		int material=Integer.parseInt(request.getParameter("material"));
		
		UserMaterialDaoImpl dao=new UserMaterialDaoImpl();
			dao.deleteUserMaterial(user, material);
		
		List<UserMaterial> um=dao.listUserMaterials();
		HttpSession session = request.getSession(false);
		 if(session!=null)  
		     session.setAttribute("userMaterialList", um); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/returnedByUser.jsp").forward(request,response);
	}

}
