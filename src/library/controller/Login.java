package library.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.UserDaoImpl;
import library.model.User;

@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Login() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		if (session.getAttribute("user") != null) { 
			User u=(User) session.getAttribute("user");
			if(u.getType().equalsIgnoreCase("admin"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/admin/home.jsp").forward(request,response);
			else if(u.getType().equalsIgnoreCase("librarian"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/home.jsp").forward(request,response);
			else if(u.getType().equalsIgnoreCase("patron"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/patron/home.jsp").forward(request,response);
			  return;
		}else{
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request,response);
			  return;
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session.getAttribute("user") != null) { 
			User u=(User) session.getAttribute("user");
			if(u.getType().equalsIgnoreCase("admin"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/admin/home.jsp").forward(request,response);
			else if(u.getType().equalsIgnoreCase("librarian"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/home.jsp").forward(request,response);
			else if(u.getType().equalsIgnoreCase("patron"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/patron/home.jsp").forward(request,response);
			  return;
		}
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		UserDaoImpl daoImpl = new UserDaoImpl();
		 PrintWriter out = response.getWriter();    

		User dbUser = daoImpl.getGetUserByUsername(username);
		if (dbUser != null && dbUser.getPassword().equals(password)) {
			 if(session!=null)  
			     session.setAttribute("user", dbUser); 
			 if(dbUser.getType().equalsIgnoreCase("admin"))
					getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/admin/home.jsp").forward(request,response);
				else if(dbUser.getType().equalsIgnoreCase("librarian"))
					getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/home.jsp").forward(request,response);
				else if(dbUser.getType().equalsIgnoreCase("patron"))
					getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/patron/home.jsp").forward(request,response);
				  return;
		} else {
			out.print("<p style=\"color:red\">Sorry username or password error</p>");
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/index.jsp");
			rd.include(request, response);
		}

		response.getWriter();
	}

}
