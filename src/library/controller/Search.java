package library.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.MaterialDaoImpl;
import library.model.Material;

/**
 * Servlet implementation class Search
 */
@WebServlet("/search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Search() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		MaterialDaoImpl dao=new MaterialDaoImpl();
		List<Material> materialList=dao.listMaterials();
		 if(session!=null)  
		     session.setAttribute("materialSearchList", materialList);  
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/patron/search.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		HttpSession session = request.getSession(false);
		MaterialDaoImpl dao=new MaterialDaoImpl();
		List<Material> materialList=dao.searchWithName(name);
		 if(session!=null)  
		     session.setAttribute("materialSearchList", materialList); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/patron/search.jsp").forward(request,response);
	}

}
