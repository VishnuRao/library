package library.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.UserDaoImpl;
import library.model.User;

@WebServlet("/listUsers")
public class ListUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		UserDaoImpl dao=new UserDaoImpl();
		List<User> userList=dao.listUsers();
		 if(session!=null)  
		     session.setAttribute("userList", userList); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/admin/userList.jsp").forward(request,response);
	}

}
