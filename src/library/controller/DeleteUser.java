package library.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.UserDaoImpl;
import library.model.User;
@WebServlet("/deleteUser")
public class DeleteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 PrintWriter out = response.getWriter();    

		String username=request.getParameter("username");
		UserDaoImpl daoImpl = new UserDaoImpl();
		System.out.println("Deleting user: "+username);
		int status=daoImpl.deleteUser(username);
		if(status>=1){
			out.print("<p style=\"color:green\">User Deleted successfully</p>");
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/admin/userList.jsp");
			rd.include(request, response);
			return;
		}else{
			out.print("<p style=\"color:red\">Unable to delete user</p>");
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/admin/userList.jsp");
			rd.include(request, response);
			return;
		}
	}

}
