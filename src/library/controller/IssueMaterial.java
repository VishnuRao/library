package library.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.MaterialDaoImpl;
import library.Dao.UserDaoImpl;
import library.Dao.UserMaterialDaoImpl;
import library.model.Material;
import library.model.User;
import library.model.UserMaterial;


@WebServlet("/issueMaterial")
public class IssueMaterial extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		MaterialDaoImpl daoMat=new MaterialDaoImpl();
		UserDaoImpl dao=new UserDaoImpl();
		List<User> userList=dao.listUsers();
		List<Material> materialList=daoMat.listAvailable();
		 if(session!=null) { 
		     session.setAttribute("mat", materialList); 
		     session.setAttribute("use", userList); 
		 }
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/issueMaterial.jsp").forward(request,response);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int userId=Integer.parseInt(req.getParameter("user"));
		int materialId=Integer.parseInt(req.getParameter("material"));
		
		UserMaterialDaoImpl dao=new UserMaterialDaoImpl();
		UserMaterial um=new UserMaterial(userId,materialId);
		dao.addUserMaterial(um);
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/issueMaterial.jsp").forward(req,resp);
	}
	
}
