package library.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.UserDaoImpl;
import library.model.User;
@WebServlet("/home")
public class Default extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Default() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session.getAttribute("user") != null) { 
			User u=(User) session.getAttribute("user");
			if(u.getType().equalsIgnoreCase("admin"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/admin/home.jsp").forward(request,response);
			else if(u.getType().equalsIgnoreCase("librarian"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/librarian/home.jsp").forward(request,response);
			else if(u.getType().equalsIgnoreCase("patron"))
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/patron/home.jsp").forward(request,response);
			else getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request,response);
			  return;
		}	  
	}
}
