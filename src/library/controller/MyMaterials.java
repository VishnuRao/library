package library.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.MaterialDaoImpl;
import library.Dao.UserDaoImpl;
import library.model.Material;
import library.model.User;

@WebServlet("/myMaterials")
public class MyMaterials extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		MaterialDaoImpl dao=new MaterialDaoImpl();
		User u=(User) session.getAttribute("user");
		System.out.println("@@ID"+u.getId());
		List<Material> materialList=dao.listMyMaterials(u.getId());
		 if(session!=null)  
		     session.setAttribute("myMaterialList", materialList); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/patron/myMaterials.jsp").forward(request,response);
	}

}
