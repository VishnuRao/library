package library.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.Dao.UserDaoImpl;
import library.model.User;

/**
 * Servlet implementation class Register
 */
@WebServlet("/register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/registration.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();    
		String name = request.getParameter("name");
		String mobile = request.getParameter("mobile");
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		String type = "patron";
		if(request.getParameter("type")!=null)
			type=request.getParameter("type");
		User user= new User(username,password,mobile,name,type);
		UserDaoImpl daoImpl = new UserDaoImpl();
		if(daoImpl.addUser(user)>=1){
			out.print("<p style=\"color:green\">Registration success please login to view your account</p>");
			HttpSession session = request.getSession(true);
			User u=(User) session.getAttribute("user");
			if(u.getType().equalsIgnoreCase("admin")){
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/admin/registration.jsp");
				rd.include(request, response);
			}else{
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/registration.jsp");
				rd.include(request, response);
			}
		}else{
			out.print("<p style=\"color:red\">Please enter proper details</p>");
			HttpSession session = request.getSession(true);
			User u=(User) session.getAttribute("user");
			if(u.getType().equalsIgnoreCase("admin")){
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/admin/registration.jsp");
				rd.include(request, response);
			}else{
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/registration.jsp");
				rd.include(request, response);
			}
		}
	}

}
